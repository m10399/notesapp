package com.example.notesappjava;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import androidx.core.view.WindowCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.notesappjava.databinding.ActivityMainBinding;

import android.view.Menu;
import android.view.MenuItem;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toolbar;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration appBarConfiguration;
    private ActivityMainBinding binding;
    private CreateNoteDialog createNoteDialog;
    private LinearLayout linearLayout;
    private TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (getIntent().getExtras() != null){
            String lastEditedNoteName = getIntent().getExtras().getString("name");
            String lastEditedNoteText = getIntent().getExtras().getString("text");
            SaveNote(null, lastEditedNoteName, lastEditedNoteText);

        }
        linearLayout = findViewById(R.id.linear_layout);

//        binding = ActivityMainBinding.inflate(getLayoutInflater());
//        setContentView(binding.getRoot());
//
//        setSupportActionBar(binding.toolbar);
//
//        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
//        appBarConfiguration = new AppBarConfiguration.Builder(navController.getGraph()).build();
//        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);


        linearLayout = findViewById(R.id.linear_layout);
        FloatingActionButton fab = findViewById(R.id.fab);
        setSupportActionBar(findViewById(R.id.toolbar));

        createNoteDialog = new CreateNoteDialog(this);
        fab.setOnClickListener(view -> createNoteDialog.show());
        createNoteDialog.setDialogResult(result -> CreateNote(result,"",true));
        CreateNotesList();
    }
    private void CreateNote(String noteName, String noteText, boolean isNewNote){
        NoteListItem noteListItem = new NoteListItem(this);
        noteListItem.NoteName = noteName;
        noteListItem.NoteText = noteText;
        noteListItem.setDialogResult(result -> RefreshNotesList());
        TextView textView = noteListItem.findViewById(R.id.note_name);
        textView.setText(noteName);
        if (isNewNote){
            SaveNote(noteListItem, noteName, "");
        }
        linearLayout.addView(noteListItem);
    }
    private void SaveNote(NoteListItem noteListItem, String noteName, String noteText){
        File file = new File(MainActivity.this.getFilesDir(), "data.txt");
        try{
            FileInputStream is = new FileInputStream(file);
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder inputBuffer = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null){
                inputBuffer.append(line).append('\n');
            }
            String inputStr = inputBuffer.toString();
            String text = "";
            if(inputStr.contains(noteName)){
                FileOutputStream writer = openFileOutput(file.getName(),Context.MODE_PRIVATE);
                int startIndex = inputStr.indexOf(noteName + ":")-1;
                int startAt = startIndex + noteName.length() + 1;
                for (int i = 0; i < inputStr.length(); i++){
                    text += inputStr.charAt(i);
                   if (i == startAt){
                       text += noteText;
                   }
                }
                writer.write(text.getBytes());
                writer.flush();
                writer.close();
            }else{
                FileOutputStream writer = openFileOutput(file.getName(),Context.MODE_APPEND);
                text = "{" + noteName + ":" + noteText + "}" + "\n";
                writer.write(text.getBytes());
                writer.flush();
                writer.close();

            }
        }
        catch (IOException e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        if (noteListItem != null){
            noteListItem.NoteName = noteName;
            noteListItem.NoteText = noteText;
        }
    }
    private void CreateNotesList(){
        File file = new File(MainActivity.this.getFilesDir(), "data.txt");
        try{
            if (!file.exists()){
                file.createNewFile();
            }
            FileInputStream is = new FileInputStream(file);
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String Line;
            while((Line = reader.readLine())!= null){
                CreateNote(Line.substring(1, Line.indexOf(":")), Line.substring(Line.indexOf(":")+1, Line.indexOf("}")), false);
                reader.close();
            }
        }
        catch (IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
    private void RefreshNotesList(){
        linearLayout.removeAllViews();
        CreateNotesList();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        return NavigationUI.navigateUp(navController, appBarConfiguration)
                || super.onSupportNavigateUp();
    }
}