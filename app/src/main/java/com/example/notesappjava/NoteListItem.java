package com.example.notesappjava;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.widget.ImageButton;
import java.io.File;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;


public class NoteListItem extends ConstraintLayout {
    public String NoteName;
    public String NoteText;
    private OnDeleteNoteDialogResult dialogResult;
    public NoteListItem(@NonNull Context context){
        super(context);
        LayoutInflater.from(getContext()).inflate(R.layout.note_list_item, this);
        ImageButton editButton = findViewById(R.id.editButton);
        ImageButton deleteButton = findViewById(R.id.deleteButton);
        editButton.setOnClickListener(v-> {
            Intent intent = new Intent(getContext(),NoteDetailActivity.class);
            intent.putExtra("name",NoteName);
            intent.putExtra("text",NoteText);
            getContext().startActivity(intent);
        });
        deleteButton.setOnClickListener(v->{
            ShowConfirmDialogBeforeDeleting();
        });

    }
    private void ShowConfirmDialogBeforeDeleting(){
        new AlertDialog.Builder(getContext())
                .setTitle(getContext().getString(R.string.confirm_delete_message))
                .setMessage(getContext().getString(R.string.confirm_delete_message))
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes,(dialog,whichButton) ->{
                    DeleteNote(NoteName);
                    if (dialogResult != null)
                        dialogResult.finish("");
                }).setNegativeButton(android.R.string.no,(dialog,whichButton) ->{
                    dialog.cancel();
                }).show();
    }
    public void setDialogResult(OnDeleteNoteDialogResult dialogResult){
        this.dialogResult = dialogResult;
    }
    public interface OnDeleteNoteDialogResult{
        void finish(String s);
    }
    private void DeleteNote(final String noteName){
        File file = new File(getContext().getFilesDir(),"data.txt");
        try {
            File tempFile = new File(file.getAbsolutePath()+ ".tmp");
            BufferedReader br = new BufferedReader(new FileReader(file));
            PrintWriter pw = new PrintWriter(new FileWriter(tempFile));
            String line;
            while ((line = br.readLine()) != null){
                if (!line.contains("{" + noteName + ":")){
                    pw.println(line);
                    pw.flush();
                }
            }
            pw.close();
            br.close();
            if (!file.delete()){
                System.out.println("Could not delete file");
                return;
            }
            if (!tempFile.renameTo(file)){
                System.out.println("Could not rename file");
            }
        }
        catch (FileNotFoundException e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        catch (IOException e){
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
