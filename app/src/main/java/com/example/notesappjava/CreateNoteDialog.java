package com.example.notesappjava;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;


public class CreateNoteDialog  extends Dialog{
    private OnCreateNoteDialogResult dialogResult;
    CreateNoteDialog(Context context){
        super(context);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.create_new_note_dialog);
        Button confirmButton = findViewById(R.id.confirm_button);
        EditText editText = findViewById(R.id.input_field);
       confirmButton.setOnClickListener(v-> {
           if (dialogResult != null)
               dialogResult.finish(String.valueOf(editText.getText()));
           editText.setText("");
           CreateNoteDialog.this.dismiss();
       });
    }
    void setDialogResult(OnCreateNoteDialogResult dialogResult){
        this.dialogResult = dialogResult;
    }
    public interface OnCreateNoteDialogResult{
        void finish(String result);
    }
}
