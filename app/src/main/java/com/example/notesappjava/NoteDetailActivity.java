package com.example.notesappjava;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class NoteDetailActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@NonNull Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.note_detail_activity);
        String noteName = getIntent().getExtras().getString("name");
        String noteText = getIntent().getExtras().getString("text");
        TextView textView = findViewById(R.id.note_text);
        Button saveButton = findViewById(R.id.save_button);
        textView.setText(noteName);
        TextView textView2 = findViewById(R.id.note_text);
        textView2.setText(noteText);
        saveButton.setOnClickListener(v->{
            Intent intent = new Intent(getApplicationContext(),MainActivity.class);
            intent.putExtra("text",textView2.getText().toString());
            intent.putExtra("name",noteName);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(intent);

        });
    }
}
